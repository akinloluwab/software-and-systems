﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Emulator
{
    class Commands
    {
        public enum Flags { mov, add, sub, and, orr, mul, div, mvns, cmp, xor, ldr, str, beq, blt, bgt, b, main, here, exit, defaults };
        OrderedDictionary registersMap = new OrderedDictionary(); // dictionary that contains the key-value pair of registers
        string r0 = "r0"; string r1 = "r1"; string r2 = "r2"; string r3 = "r3"; string r4 = "r4"; string r5 = "r5"; string r6 = "r6"; string r7 = "r7"; string r8 = "r8"; string r9 = "r9";
        string r10 = "r10"; string r11 = "r11"; string r12 = "r12"; string r13 = "r13"; string r14 = "r14"; string r15 = "r15";
        List<string> multiline = new List<string>();
        OrderedDictionary labelsMap = new OrderedDictionary();
        int counter = 0;
        string main = "main";
        string opCode = "";
        int pcCounter = 0;
        OrderedDictionary memory = new OrderedDictionary();
      
        public Commands() //constructor for the keys and values OrderedDictionary registerMap 
        {
            //(r0-r15 represent registers with default values 0)
            registersMap.Add(r0, 0); 
            registersMap.Add(r1, 0);
            registersMap.Add(r2, 0); 
            registersMap.Add(r3, 0); 
            registersMap.Add(r4, 0); 
            registersMap.Add(r5, 0); 
            registersMap.Add(r6, 0); 
            registersMap.Add(r7, 0); 
            registersMap.Add(r8, 0);
            registersMap.Add(r9, 0); 
            registersMap.Add(r10, 0); 
            registersMap.Add(r11, 0); 
            registersMap.Add(r12, 0); 
            registersMap.Add(r13, 0);
            registersMap.Add(r14, 0); 
            registersMap.Add(r15, 0); 
        } 
        
        public int registersSplitZero(int result, string[] registersSplit)
        {
            //assigns int values to the value of keys of registersMap
            if (registersSplit[0] == "r0") //r0,r1,r2 registersSplit[0] = r0
            {
                registersMap[r0] = result; // r0 = int result. registersMap[r0] obtains the value of key r0 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r1")
            {
                registersMap[r1] = result; // r1 = int result. registersMap[r1] obtains the value of key r1 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r2")
            {
                registersMap[r2] = result; // r2 = int result. registersMap[r2] obtains the value of key r2 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r3")
            {
                registersMap[r3] = result; // r3 = int result. registersMap[r3] obtains the value of key r3 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r4")
            {
                registersMap[r4] = result; // r4 = int result. registersMap[r4] obtains the value of key r4 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r5")
            {
                registersMap[r5] = result; // r5 = int result. registersMap[r5] obtains the value of key r5 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r6")
            {
                registersMap[r6] = result; // r6 = int result. registersMap[r6] obtains the value of key r6 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r7")
            {
                registersMap[r7] = result; // r7 = int result. registersMap[r7] obtains the value of key r7 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r8")
            {
                registersMap[r8] = result; // r8 = int result. registersMap[r8] obtains the value of key r8 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r9")
            {
                registersMap[r9] = result; // r9 = int result. registersMap[r9] obtains the value of key r9 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r10")
            {
                registersMap[r10] = result; // r10 = int result. registersMap[r10] obtains the value of key r10 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r11")
            {
                registersMap[r11] = result; // r11 = int result. registersMap[r11] obtains the value of key r11 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r12")
            {
                registersMap[r12] = result; // r12 = int result. registersMap[r12] obtains the value of key r12 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r13")
            {
                registersMap[r13] = result; // r13 = int result. registersMap[r13] obtains the value of key r13 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r14")
            {
                registersMap[r14] = result; // r14 = int result. registersMap[r14] obtains the value of key r14 in OrderedDictionary registersMap
            }
            else if (registersSplit[0] == "r15")
            {
                MessageBox.Show("r15 is a Program Counter"); // register r15 is declared a program counter
            }
            else
            {
                MessageBox.Show("Not a register");
            }
            return result;
        }
        public int registersSplitOneMovBr(int lhs, string[] registersSplit)
        {
            //assigns int values to the value of keys of registersMap
            if (registersSplit[1] == "[r0]") //r2,r0,r1 registersSplit[1] = r0
            {
                registersMap[r0] = lhs; // r0 = int lhs. registersMap[r0] obtains the value of key r0 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r1]") 
            {
                registersMap[r1] = lhs; // r1 = int lhs. registersMap[r1] obtains the value of key r1 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r2]")
            {
                registersMap[r2] = lhs; // r2 = int lhs. registersMap[r2] obtains the value of key r2 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r3]")
            {
                registersMap[r3] = lhs; // r3 = int lhs. registersMap[r3] obtains the value of key r3 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r4]")
            {
                registersMap[r4] = lhs; // r4 = int lhs. registersMap[r4] obtains the value of key r4 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r5]")
            {
                registersMap[r5] = lhs; // r5 = int lhs. registersMap[r5] obtains the value of key r5 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r6]")
            {
                registersMap[r6] = lhs; // r6 = int lhs. registersMap[r6] obtains the value of key r6 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r7]")
            {
                registersMap[r7] = lhs; // r7 = int lhs. registersMap[r7] obtains the value of key r7 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r8]")
            {
                registersMap[r8] = lhs; // r8 = int lhs. registersMap[r8] obtains the value of key r8 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r9]")
            {
                registersMap[r9] = lhs; // r9 = int lhs. registersMap[r9] obtains the value of key r9 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r10]")
            {
                registersMap[r10] = lhs; // r10 = int lhs. registersMap[r10] obtains the value of key r10 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r11]")
            {
                registersMap[r11] = lhs; // r11 = int lhs. registersMap[r11] obtains the value of key r11 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r12]")
            {
                registersMap[r12] = lhs; // r12 = int lhs. registersMap[r12] obtains the value of key r12 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r13]")
            {
                registersMap[r13] = lhs; // r13 = int lhs. registersMap[r13] obtains the value of key r13 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r14]")
            {
                registersMap[r14] = lhs; // r14 = int lhs. registersMap[r14] obtains the value of key r14 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "[r15]")
            {
                MessageBox.Show("r15 is a Program Counter"); // register r15 is declared a program counter
            }
            return lhs;
        }
        public int registersSplitOneMov(int lhs, string[] registersSplit)
        {
            //assigns int values to the value of keys of registersMap
            if (registersSplit[1] == "r0")
            {
                registersMap[r0] = lhs; // r0 = int lhs. registersMap[r0] obtains the value of key r0 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r1")
            {
                registersMap[r1] = lhs; // r1 = int lhs. registersMap[r1] obtains the value of key r1 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r2")
            {
                registersMap[r2] = lhs; // r2 = int lhs. registersMap[r2] obtains the value of key r2 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r3")
            {
                registersMap[r3] = lhs; // r3 = int lhs. registersMap[r3] obtains the value of key r3 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r4")
            {
                registersMap[r4] = lhs; // r4 = int lhs. registersMap[r4] obtains the value of key r4 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r5")
            {
                registersMap[r5] = lhs; // r5 = int lhs. registersMap[r5] obtains the value of key r5 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r6")
            {
                registersMap[r6] = lhs; // r6 = int lhs. registersMap[r6] obtains the value of key r6 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r7")
            {
                registersMap[r7] = lhs; // r7 = int lhs. registersMap[r7] obtains the value of key r7 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r8")
            {
                registersMap[r8] = lhs; // r8 = int lhs. registersMap[r8] obtains the value of key r8 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r9")
            {
                registersMap[r9] = lhs; // r9 = int lhs. registersMap[r9] obtains the value of key r9 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r10")
            {
                registersMap[r10] = lhs; // r10 = int lhs. registersMap[r10] obtains the value of key r10 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r11")
            {
                registersMap[r11] = lhs; // r11 = int lhs. registersMap[r11] obtains the value of key r11 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r12")
            {
                registersMap[r12] = lhs; // r12 = int lhs. registersMap[r12] obtains the value of key r12 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r13")
            {
                registersMap[r13] = lhs; // r13 = int lhs. registersMap[r13] obtains the value of key r13 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r14")
            {
                registersMap[r14] = lhs; // r14 = int lhs. registersMap[r14] obtains the value of key r14 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r15")
            {
                MessageBox.Show("r15 is a Program Counter");
            }
            return lhs;
        }        
        public int registersSplitOne(int lhs, string[] registersSplit)
        {//assigns int values to the value of keys of registersMap
            if (registersSplit[1] == "r0")
            {
                registersMap[r0] = lhs; // r1 = int lhs. registersMap[r1] obtains the value of key r1 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r1")
            {
                registersMap[r1] = lhs;
            }
            else if (registersSplit[1] == "r2")
            {
                registersMap[r2] = lhs;
            }
            else if (registersSplit[1] == "r3")
            {
                registersMap[r3] = lhs;
            }
            else if (registersSplit[1] == "r4")
            {
                registersMap[r4] = lhs;
            }
            else if (registersSplit[1] == "r5")
            {
                registersMap[r5] = lhs;
            }
            else if (registersSplit[1] == "r6")
            {
                registersMap[r6] = lhs;
            }
            else if (registersSplit[1] == "r7")
            {
                registersMap[r7] = lhs;
            }
            else if (registersSplit[1] == "r8")
            {
                registersMap[r8] = lhs;
            }
            else if (registersSplit[1] == "r9")
            {
                registersMap[r9] = lhs;
            }
            else if (registersSplit[1] == "r10")
            {
                registersMap[r10] = lhs; // r10 = int lhs. registersMap[r10] obtains the value of key r10 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r11")
            {
                registersMap[r11] = lhs; // r11 = int lhs. registersMap[r11] obtains the value of key r11 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r12")
            {
                registersMap[r12] = lhs; // r12 = int lhs. registersMap[r12] obtains the value of key r12 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r13")
            {
                registersMap[r13] = lhs; // r13 = int lhs. registersMap[r13] obtains the value of key r13 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r14")
            {
                registersMap[r14] = lhs; // r14 = int lhs. registersMap[r14] obtains the value of key r14 in OrderedDictionary registersMap
            }
            else if (registersSplit[1] == "r15")
            {
                MessageBox.Show("r15 is a Program Counter");
            }
            else
            {
                MessageBox.Show("Not a register"); // r15 is declared a program counter
            }
            return lhs;
        }
        public int registersSplitTwo(int rhs, string[] registersSplit)
        {//assigns int values to the value of keys of registersMap
            if (registersSplit[2] == "r0")
            {
                registersMap[r0] = rhs; // r0 = int rhs. registersMap[r0] obtains the value of key r0 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r1")
            {
                registersMap[r1] = rhs; // r1 = int rhs. registersMap[r1] obtains the value of key r1 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r2")
            {
                registersMap[r2] = rhs; // r2 = int rhs. registersMap[r2] obtains the value of key r2 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r3")
            {
                registersMap[r3] = rhs; // r3 = int rhs. registersMap[r3] obtains the value of key r3 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r4")
            {
                registersMap[r4] = rhs; // r4 = int rhs. registersMap[r4] obtains the value of key r4 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r5")
            {
                registersMap[r5] = rhs; // r5 = int rhs. registersMap[r5] obtains the value of key r5 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r6")
            {
                registersMap[r6] = rhs; // r6 = int rhs. registersMap[r6] obtains the value of key r6 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r7")
            {
                registersMap[r7] = rhs; // r7 = int rhs. registersMap[r7] obtains the value of key r7 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r8")
            {
                registersMap[r8] = rhs; // r8 = int rhs. registersMap[r8] obtains the value of key r8 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r9")
            {
                registersMap[r9] = rhs; // r9 = int rhs. registersMap[r9] obtains the value of key r9 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r10")
            {
                registersMap[r10] = rhs; // r10 = int rhs. registersMap[r10] obtains the value of key r10 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r11")
            {
                registersMap[r11] = rhs; // r11 = int rhs. registersMap[r11] obtains the value of key r11 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r12")
            {
                registersMap[r12] = rhs; // r12 = int rhs. registersMap[r12] obtains the value of key r12 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r13")
            {
                registersMap[r13] = rhs; // r13 = int rhs. registersMap[r13] obtains the value of key r13 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r14")
            {
                registersMap[r14] = rhs; // r14 = int rhs. registersMap[r14] obtains the value of key r14 in OrderedDictionary registersMap
            }
            else if (registersSplit[2] == "r15")
            {
                MessageBox.Show("r15 is a Program Counter"); // r15 is declared a program counter
            }
            
            return rhs;
        }        
        public int registerFormat(string registerKey, OrderedDictionary regDictionary)
        {
            int r = 0;
            if (regDictionary.Contains(registerKey))
            {
                r = (int)regDictionary[registerKey]; // assigns the value of the register key to int r
            }
            else
            {
                try
                {
                    r = Int32.Parse(registerKey); // receives integer values
                }
                catch (Exception e)
                {
                    MessageBox.Show("Not a valid register");
                }
            }
            return r;
        }
        public string immediateOperand(string[] registersSplit)
        {// I = 1 if operand2 is a register and 3 if operand2 is not in registersSplit[1]
            string I = "";
            if (registersSplit[1].Contains("#"))
            {
                I = "3";
            }
            else
            {
                I = "1";
            }
            return I;
        }
        public string immediateOperand1(string[] registersSplit)
        {// I = 1 if operand2 is a register and 3 if operand2 is not in registersSplit[2]
            string I = "";
            if (registersSplit[2].Contains("#"))
            {
                I = "3";
            }
            else
            {
                I = "1";
            }
            return I;
        }
        public string immediateOperand0(string[] registersSplit)
        {// I = 0 if operand2 is a register and 2 if operand2 is not in registersSplit[1]
            string I = "";
            if (registersSplit[1].Contains("#"))
            {
                I = "2";
            }
            else
            {
                I = "0";
            }
            return I;
        }
        public string immediateOperand2(string[] registersSplit)
        {// I = 0 if operand2 is a register and 2 if operand2 is not in registersSplit[2]
            string I = "";
            if (registersSplit[2].Contains("#"))
            {
                I = "2";
            }
            else
            {
                I = "0";
            }
            return I;
        }
        public string registersSplitOpCode(string[] registersSplit, int number)
        {
            //assigns hex values to the value of keys of registersMap
            string result = "";
            if (registersSplit[2] == "r0") //r0,r1,r2 registersSplit[0] = r0
            {
                result = "000";
            }
            else if (registersSplit[2] == "r1")
            {
                result = "001";
            }
            else if (registersSplit[2] == "r2")
            {
                result = "002";
            }
            else if (registersSplit[2] == "r3")
            {
                result = "003";
            }
            else if (registersSplit[2] == "r4")
            {
                result = "004";
            }
            else if (registersSplit[2] == "r5")
            {
                result = "005";
            }
            else if (registersSplit[2] == "r6")
            {
                result = "006";
            }
            else if (registersSplit[2] == "r7")
            {
                result = "007";
            }
            else if (registersSplit[2] == "r8")
            {
                result = "008";
            }
            else if (registersSplit[2] == "r9")
            {
                result = "009";
            }
            else if (registersSplit[2] == "r10")
            {
                result = "00A";
            }
            else if (registersSplit[2] == "r11")
            {
                result = "00B";
            }
            else if (registersSplit[2] == "r12")
            {
                result = "00C";
            }
            else if (registersSplit[2] == "r13")
            {
                result = "00D";
            }
            else if (registersSplit[2] == "r14")
            {
                result = "00E";
            }
            else if (registersSplit[2] == "r15")
            {
                result = "00F";
            }
            else
            {                
                string hexNumb = number.ToString("X");
                result = hexNumb.PadLeft(3, '0');
            }
            return result;
        }
        public string registersSplitOpCodee(string[] registersSplit, int number)
        {
            //assigns hex values to the value of keys of registersMap
            string result = "";
            if (registersSplit[1] == "r0") //r0,r1,r2 registersSplit[0] = r0
            {
                result = "000";
            }
            else if (registersSplit[1] == "r1")
            {
                result = "001";
            }
            else if (registersSplit[1] == "r2")
            {
                result = "002";
            }
            else if (registersSplit[1] == "r3")
            {
                result = "003";
            }
            else if (registersSplit[1] == "r4")
            {
                result = "004";
            }
            else if (registersSplit[1] == "r5")
            {
                result = "005";
            }
            else if (registersSplit[1] == "r6")
            {
                result = "006";
            }
            else if (registersSplit[1] == "r7")
            {
                result = "007";
            }
            else if (registersSplit[1] == "r8")
            {
                result = "008";
            }
            else if (registersSplit[1] == "r9")
            {
                result = "009";
            }
            else if (registersSplit[1] == "r10")
            {
                result = "00A";
            }
            else if (registersSplit[1] == "r11")
            {
                result = "00B";
            }
            else if (registersSplit[1] == "r12")
            {
                result = "00C";
            }
            else if (registersSplit[1] == "r13")
            {
                result = "00D";
            }
            else if (registersSplit[1] == "r14")
            {
                result = "00E";
            }
            else if (registersSplit[1] == "r15")
            {
                result = "00F";
            }
            else
            {
                string hexNumb = number.ToString("X");
                result = hexNumb.PadLeft(3, '0');
            }
            return result;
        }
        public string registersSplitOpCodeOne(string[] registersSplit)
        {
            //assigns hex values to the value of keys of registersMap
            string result = "";
            if (registersSplit[1] == "r0") //r0,r1,r2 registersSplit[0] = r0
            {
                result = "0";
            }
            else if (registersSplit[1] == "r1")
            {
                result = "1";
            }
            else if (registersSplit[1] == "r2")
            {
                result = "2";
            }
            else if (registersSplit[1] == "r3")
            {
                result = "3";
            }
            else if (registersSplit[1] == "r4")
            {
                result = "4";
            }
            else if (registersSplit[1] == "r5")
            {
                result = "5";
            }
            else if (registersSplit[1] == "r6")
            {
                result = "6";
            }
            else if (registersSplit[1] == "r7")
            {
                result = "7";
            }
            else if (registersSplit[1] == "r8")
            {
                result = "8";
            }
            else if (registersSplit[1] == "r9")
            {
                result = "9";
            }
            else if (registersSplit[1] == "r10")
            {
                result = "A";
            }
            else if (registersSplit[1] == "r11")
            {
                result = "B";
            }
            else if (registersSplit[1] == "r12")
            {
                result = "C";
            }
            else if (registersSplit[1] == "r13")
            {
                result = "D";
            }
            else if (registersSplit[1] == "r14")
            {
                result = "E";
            }
            else if (registersSplit[1] == "r15")
            {
                result = "F";
            }
            return result;
        }
        public string registersSplitOpCodeZero(string[] registersSplit)
        {
            //assigns hex values to the value of keys of registersMap
            string result = "";
            if (registersSplit[0] == "r0") //r0,r1,r2 registersSplit[0] = r0
            {
                result = "0";
            }
            else if (registersSplit[0] == "r1")
            {
                result = "1";
            }
            else if (registersSplit[0] == "r2")
            {
                result = "2";
            }
            else if (registersSplit[0] == "r3")
            {
                result = "3";
            }
            else if (registersSplit[0] == "r4")
            {
                result = "4";
            }
            else if (registersSplit[0] == "r5")
            {
                result = "5";
            }
            else if (registersSplit[0] == "r6")
            {
                result = "6";
            }
            else if (registersSplit[0] == "r7")
            {
                result = "7";
            }
            else if (registersSplit[0] == "r8")
            {
                result = "8";
            }
            else if (registersSplit[0] == "r9")
            {
                result = "9";
            }
            else if (registersSplit[0] == "r10")
            {
                result = "A";
            }
            else if (registersSplit[0] == "r11")
            {
                result = "B";
            }
            else if (registersSplit[0] == "r12")
            {
                result = "C";
            }
            else if (registersSplit[0] == "r13")
            {
                result = "D";
            }
            else if (registersSplit[0] == "r14")
            {
                result = "E";
            }
            else if (registersSplit[0] == "r15")
            {
                result = "F";
            }
            return result;
        }
        public Flags AssignFlag(string line)
        {//checks for the instructions and executes it appropriately. 
            Flags flag = Flags.defaults;
            if (line.StartsWith("add")) //if a line starts with add,it executes what is in add
            {
                flag = Flags.add;
            }
            else if (line.StartsWith("sub")) // if a line starts with sub,it executes what is in sub
            {
                flag = Flags.sub;
            }
            else if (line.StartsWith("mul")) //if a line starts with mul,it executes what is in mul
            {
                flag = Flags.mul;
            }
            else if (line.StartsWith("div")) // if a line starts with div,it executes what is in div
            {
                flag = Flags.div;
            }
            else if (line.StartsWith("mov")) // if a line starts with mov,it executes what is in mov
            {
                flag = Flags.mov;
            }
            else if (line.StartsWith("and")) // if a line starts with and, it executes what is in and
            {
                flag = Flags.and;
            }
            else if (line.StartsWith("or")) // if a line starts with or,it executes what is in orr
            {
                flag = Flags.orr;
            }
            else if (line.StartsWith("mvns")) // if a line starts with inv,it executes what is in mvns
            {
                flag = Flags.mvns;
            }
            else if (line.StartsWith("xor")) // if a line starts with xor,it executes what is in xor
            {
                flag = Flags.xor;
            }
            else if (line.StartsWith("ldr")) // if a line starts with ldr,it executes what is in ldr
            {
                flag = Flags.ldr;
            }
            else if (line.StartsWith("str")) // if a line starts with str,it executes what is in strb
            {
                flag = Flags.str;
            }
            else if (line.StartsWith("cmp")) // if a line starts with cmp,it executes what is in cmp
            {
                flag = Flags.cmp;
            }
            else if (line.StartsWith("bgt")) // if a line starts with bgt,it executes what is in bgt
            {
                flag = Flags.bgt;
            }
            else if (line.StartsWith("blt")) // if a line starts with blt,it executes what is in blt
            {
                flag = Flags.blt;
            }
            else if (line.StartsWith("beq")) // if a line starts with beq,it executes what is in beq
            {
                flag = Flags.beq;
            }
            else if (line.StartsWith("b")) // if a line starts with b,it executes what is in b
            {
                flag = Flags.b;
            }
            else
            {
                MessageBox.Show("Invalid instruction!");
            }
            return flag;
        }
        public void update_labelMap(List<string> programlines)
        {
            for (int lineIndex = 0; lineIndex < multiline.Count; lineIndex++)
            {
                if (multiline[lineIndex].Contains(":")) //here:
                {
                    string label = multiline[lineIndex].TrimEnd(':'); //assigns the word before ':' to string label.. label = here
                    labelsMap[label] = lineIndex; // assigns the int value of counter to the value of the key of labelsMap
                }
            }
        }
        public string[] getComparisonValue(string line)
        {
            string[] command = line.Split(' '); //splits by space into array command - mov r1,r2 (command[0] = mov; command[1] = r1,r2)
            if(command.Length > 1)
            {
                string regKeysArrays = command[1];
                string[] regKeys = regKeysArrays.Split(','); //splits by comma into array regKeys - r0,r1,#22 (regKeys[0] = r0; regKeys[2] = #22)
                return regKeys;
            }
            else
            {
                string[] emptyString = { };
                return emptyString;
            }           
        }
        public void Multiline(RichTextBox richTextBox1, RichTextBox richTextBox2)
        {
            if (multiline.Count == 0)
            {
                string[] newLine = { "\r", "\n", "\r\n" };
                multiline = richTextBox1.Text.Split(newLine, StringSplitOptions.RemoveEmptyEntries).ToList<string>(); //splits each line into List multiline
                update_labelMap(multiline); //initialize labelmap
            }
                        
            Flags flag = Flags.defaults;

            if ((multiline.Count > 0) && (counter < multiline.Count))
            {
                if (multiline[counter].Contains(":"))
                {
                    main = multiline[counter].TrimEnd(':'); //assigns the string before ':' to string main
                    pcCounter = 0; // assigns zero to the program counter (PC)
                    counter++; // increment counter
                }
                int new_counter = counter;
                string line = multiline[counter]; //assigns texts of a line in richTextBox1 to line
                flag = AssignFlag(line); 
                string[] command = line.Split(' ');
                if (command.Length == 2 || command.Length >= 3 && command[2].StartsWith("@"))
                {
                    string registers = command[1];
                    string[] registersSplit = registers.Split(',');
                    
                    if (registersSplit.Length == 3)                    
                    {                       
                        switch (flag)
                        {               
                            case Flags.add:
                                if ((registersMap.Contains(registersSplit[0]) && registersMap.Contains(registersSplit[1]) && registersSplit[2].Contains("#")) ||  registersMap.Contains(registersSplit[2]))
                                {                                    
                                    int lhs = (int) registersMap[registersSplit[1]]; //stores the integer value of the register in lhs  
                                    string I = immediateOperand2(registersSplit);
                                    registersSplitOne(lhs, registersSplit); //assigns the value of lhs to the appropriate register
                                    registersSplit[2] = registersSplit[2].TrimStart('#');
                                    int rhs = registerFormat(registersSplit[2], registersMap); //gets the integer value of the register or from the # string                                    
                                    registersSplitTwo(rhs, registersSplit);  //assigns the value of rhs to the appropriate register                                 
                                    int result = (int) registersMap[registersSplit[0]];
                                    result = lhs + rhs; //r0,r1,r2; r0 = r1 + r2
                                    registersSplitZero(result, registersSplit); //assigns the value of result to the appropriate register 
                                    string regO = registersSplitOpCode(registersSplit, rhs); //operand2
                                    string regDest = registersSplitOpCodeZero(registersSplit); //destination register
                                    string regN = registersSplitOpCodeOne(registersSplit); // 1st operand register
                                    opCode = "E" + I + "8" + regN + regDest + regO; //opcode in hex form
                                }
                                else
                                {
                                    MessageBox.Show("Please input a valid register");
                                }                                                                
                                break;
                            case Flags.sub:
                                if ((registersMap.Contains(registersSplit[0]) && registersMap.Contains(registersSplit[1]) && registersSplit[2].Contains("#")) || registersMap.Contains(registersSplit[2]))
                                {
                                    int lhs = (int) registersMap[registersSplit[1]];  //stores the integer value of the register in lhs
                                    string I = immediateOperand2(registersSplit);
                                    registersSplitOne(lhs, registersSplit); //assigns the value of lhs to the appropriate register
                                    registersSplit[2] = registersSplit[2].TrimStart('#'); // only reads what is after #
                                    int rhs = registerFormat(registersSplit[2], registersMap); //gets the integer value of the register or from the # string                                   
                                    registersSplitTwo(rhs, registersSplit); //assigns the value of rhs to the appropriate register
                                    int result = (int) registersMap[registersSplit[0]]; //stores the integer value of the register in result
                                    result = lhs - rhs; //r0,r1,r2; r0 = r1 - r2
                                    registersSplitZero(result, registersSplit); //assigns the value of result to the appropriate register
                                    string regO = registersSplitOpCode(registersSplit, rhs); //operand2
                                    string regDest = registersSplitOpCodeZero(registersSplit); //destination register
                                    string regN = registersSplitOpCodeOne(registersSplit); //1st operand register
                                    opCode = "E" + I + "4" + regN + regDest + regO; //opcode in hex form
                                }
                                else
                                {
                                    MessageBox.Show("Please input a valid register");
                                }
                                break;
                            case Flags.mul:
                                opCode = "";
                                if ((registersMap.Contains(registersSplit[0]) && registersMap.Contains(registersSplit[1]) && registersSplit[2].Contains("#")) || registersMap.Contains(registersSplit[2]))
                                {
                                    int lhs = (int) registersMap[registersSplit[1]]; //stores the integer value of the register in lhs 
                                    registersSplitOne(lhs, registersSplit);  //assigns the value of lhs to the appropriate register                                  
                                    registersSplit[2] = registersSplit[2].TrimStart('#'); // only reads what is after #
                                    int rhs = registerFormat(registersSplit[2], registersMap); //gets the integer value of the register or from the # string                                   
                                    registersSplitTwo(rhs, registersSplit);  //assigns the value of rhs to the appropriate register                                
                                    int result = (int) registersMap[registersSplit[0]]; //stores the integer value of the register in result
                                    result = lhs * rhs; //r0,r1,r2; r0 = r1 x r2
                                    registersSplitZero(result, registersSplit); //assigns the value of result to the appropriate register
                                    string regO = registersSplitOpCode(registersSplit, rhs); //operand2
                                    string regDest = registersSplitOpCodeZero(registersSplit); //destination register
                                    string regN = registersSplitOpCodeOne(registersSplit); // 1st operand register
                                    opCode = "E00" + regN + regDest + regO; // opcode in hex form
                                }
                                else
                                {
                                    MessageBox.Show("Please input a valid register");
                                }
                                break;
                            case Flags.div:
                                opCode = "";
                                if ((registersMap.Contains(registersSplit[0]) && registersMap.Contains(registersSplit[1]) && registersSplit[2].Contains("#")) || registersMap.Contains(registersSplit[2]))
                                {
                                    int lhs = (int) registersMap[registersSplit[1]]; //stores the integer value of the register in lhs                                  
                                    registersSplitOne(lhs, registersSplit); //assigns the value of lhs to the appropriate register
                                    registersSplit[2] = registersSplit[2].TrimStart('#'); // only reads what is after #
                                    int rhs = registerFormat(registersSplit[2], registersMap); //gets the integer value of the register or from the # string                                  
                                    registersSplitTwo(rhs, registersSplit); //assigns the value of rhs to the appropriate register
                                    int result = (int) registersMap[registersSplit[0]]; //stores the integer value of the register in result
                                    result = lhs / rhs; //r0,r1,r2; r0 = r1 / r2
                                    registersSplitZero(result, registersSplit); //assigns the value of result to the appropriate register
                                }
                                else
                                {
                                    MessageBox.Show("Please input a valid register");
                                }
                                break;
                            case Flags.and:
                                if ((registersMap.Contains(registersSplit[0]) && registersMap.Contains(registersSplit[1]) && registersSplit[2].Contains("#")) || registersMap.Contains(registersSplit[2]))
                                {
                                    int lhs = (int)registersMap[registersSplit[1]]; //stores the integer value of the register in lhs
                                    string I = immediateOperand2(registersSplit);
                                    registersSplitOne(lhs, registersSplit); //assigns the value of lhs to the appropriate register                                   
                                    registersSplit[2] = registersSplit[2].TrimStart('#'); // only reads what is after #
                                    int rhs = registerFormat(registersSplit[2], registersMap); //gets the integer value of the register or from the # string                                  
                                    registersSplitTwo(rhs, registersSplit); //assigns the value of rhs to the appropriate register
                                    int result = (int) registersMap[registersSplit[0]]; //stores the integer value of the register in result
                                    result = lhs & rhs; //r0,r1,r2; r0 = r1 &  r2 -- bitwise and
                                    registersSplitZero(result, registersSplit); //assigns the value of result to the appropriate register
                                    string regO = registersSplitOpCode(registersSplit, rhs); //operand2
                                    string regDest = registersSplitOpCodeZero(registersSplit); //destination register
                                    string regN = registersSplitOpCodeOne(registersSplit); //1st operand register
                                    opCode = "E" + I + "0" + regN + regDest + regO; //opcode in hex form
                                }
                                else
                                {
                                    MessageBox.Show("Please input a valid register");
                                }
                                break;
                            case Flags.orr:
                                if ((registersMap.Contains(registersSplit[0]) && registersMap.Contains(registersSplit[1]) && registersSplit[2].Contains("#")) || registersMap.Contains(registersSplit[2]))
                                {
                                    int lhs = (int) registersMap[registersSplit[1]]; //stores the integer value of the register in lhs
                                    string I = immediateOperand1(registersSplit);
                                    registersSplitOne(lhs, registersSplit); //assigns the value of lhs to the appropriate register
                                    int rhs = registerFormat(registersSplit[2], registersMap); //gets the integer value of the register or from the # string                                  
                                    registersSplitTwo(rhs, registersSplit); //assigns the value of rhs to the appropriate register
                                    int result = (int) registersMap[registersSplit[0]]; //stores the integer value of the register in result
                                    result = lhs | rhs; //r0,r1,r2; r0 = r1 | r2 -- bitwise or
                                    registersSplitZero(result, registersSplit); //assigns the value of result to the appropriate register
                                    string regO = registersSplitOpCode(registersSplit, rhs); //operand2
                                    string regDest = registersSplitOpCodeZero(registersSplit); //destination register
                                    string regN = registersSplitOpCodeOne(registersSplit); //1st operand register
                                    opCode = "E" + I + "8" + regN + regDest + regO; //opcode in hex form
                                }
                                else
                                {
                                    MessageBox.Show("Please input a valid register");
                                }
                                break;
                            case Flags.xor:
                                if ((registersMap.Contains(registersSplit[0]) && registersMap.Contains(registersSplit[1]) && registersSplit[2].Contains("#")) || registersMap.Contains(registersSplit[2]))
                                {
                                    int lhs = (int) registersMap[registersSplit[1]]; //stores the integer value of the register in lhs
                                    string I = immediateOperand2(registersSplit);
                                    registersSplitOne(lhs, registersSplit); //assigns the value of lhs to the appropriate register
                                    int rhs = registerFormat(registersSplit[2], registersMap);  //gets the integer value of the register or from the # string                                  
                                    registersSplitTwo(rhs, registersSplit); //assigns the value of rhs to the appropriate register
                                    int result = (int) registersMap[registersSplit[0]]; //stores the integer value of the register in result
                                    result = lhs ^ rhs; //r0,r1,r2; r0 = r1 ^ r2 -- bitwise xor
                                    registersSplitZero(result, registersSplit); //assigns the value of result to the appropriate register
                                    string regO = registersSplitOpCode(registersSplit, rhs); //operand2
                                    string regDest = registersSplitOpCodeZero(registersSplit); //destination register
                                    string regN = registersSplitOpCodeOne(registersSplit); //1st operand register
                                    opCode = "E" + I + "2" + regN + regDest + regO; //opcode in hex form
                                }
                                else
                                {
                                    MessageBox.Show("Please input a valid register");
                                }
                                break;
                        }
                    }
                    else if (registersSplit.Length == 2)                    
                    {
                        switch (flag)
                        {
                            case Flags.mov:                                
                                if (registersMap.Contains(registersSplit[0]) && registersSplit[1].Contains("#") || registersMap.Contains(registersSplit[1]))
                                {
                                    int result =  (int) registersMap[registersSplit[0]]; //stores the integer value of the register in result
                                    string I = immediateOperand(registersSplit);
                                    registersSplit[1] = registersSplit[1].TrimStart('#');
                                    int value = registerFormat(registersSplit[1], registersMap); //gets the integer value of the register or from the # string 
                                    registersSplitOneMov(value, registersSplit); //assigns the value of value to the appropriate register
                                    result = value;
                                    registersSplitZero(result, registersSplit); //assigns the value of result to the appropriate register                                   
                                    string regO = registersSplitOpCodee(registersSplit, value); //operand
                                    string regDest = registersSplitOpCodeZero(registersSplit); //destination register
                                    opCode = "E" + I + "A0" + regDest + regO; //opcode in hex form
                                }
                                break;
                            case Flags.str:
                                {
                                    registersSplit[1] = registersSplit[1].TrimStart('[');
                                    registersSplit[1] = registersSplit[1].TrimEnd(']');
                                    int value = (int)registersMap[registersSplit[0]]; // assigns the value of the register to value
                                    int key = (int)registersMap[registersSplit[1]]; // assigns the value of the register to key
                                    memory.Add(key,value); // adds the key-value pair to orderedDictionary memory
                                    string regDest = registersSplitOpCodeZero(registersSplit); //destination register
                                    string regO = registersSplitOpCodeOne(registersSplit); //operand
                                    opCode = "E58" + regO + regDest + "000"; //opcode in hex form
                                    MessageBox.Show(value + " stored to memory[" + key + "]");  
                                }
                                break;
                            case Flags.mvns:                                
                                if (registersMap.Contains(registersSplit[0]) && registersSplit[1].Contains("#") || registersMap.Contains(registersSplit[1]))
                                {
                                    int result = (int) registersMap[registersSplit[0]];
                                    string I = immediateOperand(registersSplit);
                                    registersSplit[1] = registersSplit[1].TrimStart('#');  
                                    int value = registerFormat(registersSplit[1], registersMap); //gets the integer value of the register or from the # string 
                                    registersSplitOneMov(value, registersSplit); //assigns the value of value to the appropriate register                                  
                                    result = ~value; // inverts value and assign the result to result
                                    registersSplitZero(result, registersSplit); //assigns the value of result to the appropriate register
                                    string regO = registersSplitOpCodee(registersSplit, value); //operand
                                    string regDest = registersSplitOpCodeZero(registersSplit); //destination regiser
                                    opCode = "E" + I + "E0" + regDest + regO; //opcode in hex form
                                }
                                break;
                            case Flags.ldr:
                                opCode = "";
                                if (registersMap.Contains(registersSplit[0]) && registersSplit[1].Contains("0x"))
                                {
                                    int result = (int) registersMap[registersSplit[0]]; //assigns value of register key to result
                                    char[] trimCharacters = { '0', 'x' }; 
                                    registersSplit[1] = registersSplit[1].TrimStart(trimCharacters); // trims 0x in 0xb4dc
                                    string hexValue = registersSplit[1]; 
                                    int value = int.Parse(hexValue, System.Globalization.NumberStyles.HexNumber); //converts the hex string to integer
                                    registersSplitOneMov(value, registersSplit); // assigns the integer value to the appropriate register key
                                    result = value;
                                    registersSplitZero(result, registersSplit); // assigns the result to the appropriate register key                                    
                                }
                                else
                                {
                                    int result = (int)registersMap[registersSplit[0]];
                                    registersSplit[1] = registersSplit[1].TrimStart('['); //removes '['
                                    registersSplit[1] = registersSplit[1].TrimEnd(']'); //removes ']'
                                    int value = (int)registersMap[registersSplit[1]];
                                    registersSplitOneMovBr(value, registersSplit);
                                    try
                                    {
                                        int key = (int)memory[0];
                                        result = key;
                                        registersSplitZero(result, registersSplit); // assigns the result to the appropriate register key
                                    }
                                    catch (Exception e)
                                    {
                                        MessageBox.Show("No content has been stored in memory");
                                    }
                                    string regDest = registersSplitOpCodeZero(registersSplit); //destination register
                                    string regO = registersSplitOpCodeOne(registersSplit); //operand
                                    opCode = "E5B" + regO + regDest + "000"; // opcode in hex form
                                }
                                break;
                            
                            case Flags.cmp:
                                if (registersMap.Contains(registersSplit[0]) && registersSplit[1].Contains("#") || registersMap.Contains(registersSplit[1]))
                                {
                                    string I = immediateOperand(registersSplit);
                                    registersSplit[1] = registersSplit[1].TrimStart('#');
                                    int value = registerFormat(registersSplit[1], registersMap);
                                    int result = (int)registersMap[registersSplit[0]];
                                    string regO = registersSplitOpCodee(registersSplit, value); //operand2
                                    string regN = registersSplitOpCodeZero(registersSplit); //1st operand
                                    opCode = "E" + I + "5" + regN + "0" + regO; //opcode in hex form
                                }
                                break;
                            
                        }
                    }
                    else if (registersSplit.Length == 1)
                    {
                        switch (flag)
                        {
                            case Flags.beq:
                                opCode = "";
                                string[] compVals = getComparisonValue(multiline[counter - 1]); //previous line
                                if (multiline[counter - 1].StartsWith("cmp") && compVals.Length > 0) // if previous line starts with cmp
                                {
                                    int regName = (int)registersMap[compVals[0]]; // cmp r3,#4 (compVals[0] = r3,compVals[1] = #4) 
                                    int regChar = int.Parse(compVals[1].Trim('#'));
                                    if (regName == regChar) // if r3 = 4
                                    {
                                        new_counter = (int)labelsMap[registersSplit[0]]; // assigns the value of the labelsMap key to new_counter
                                        main = registersSplit[0]; // string main = label
                                        opCode = "EA";
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please input a valid command");
                                }
                                break;
                            case Flags.bgt:
                                opCode = "";
                                compVals = getComparisonValue(multiline[counter - 1]); // previous line
                                if (multiline[counter - 1].StartsWith("cmp") && compVals.Length > 0) // if previous line starts with cmp
                                {
                                    int regName = (int)registersMap[compVals[0]]; // cmp r3,#4 (compVals[0] = r3,compVals[1] = #4)
                                    int regChar = int.Parse(compVals[1].TrimStart('#'));
                                    if (regName > regChar) // if r3 > 4
                                    {
                                        new_counter = (int)labelsMap[registersSplit[0]]; // assigns the value of the labelsMap key to new_counter
                                        main = registersSplit[0]; // string main = label
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please input a valid command");
                                }
                                break;
                            case Flags.blt:
                                opCode = "";
                                compVals = getComparisonValue(multiline[counter - 1]); // previous line
                                if (multiline[counter - 1].StartsWith("cmp") && compVals.Length > 0) // if previous line starts with cmp
                                {
                                    int regName = (int)registersMap[compVals[0]]; // cmp r3,#4 (compVals[0] = r3,compVals[1] = #4)
                                    int regChar = int.Parse(compVals[1].Trim('#'));
                                    if (regName < regChar) // if r3 < 4
                                    {
                                        new_counter = (int)labelsMap[registersSplit[0]]; // assigns the value of the labelsMap key to new_counter
                                        main = registersSplit[0]; // string main = label
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please input a valid command");
                                }
                                break;
                            case Flags.b:
                                opCode = "";
                                new_counter = (int)labelsMap[registersSplit[0]]; // assigns the value of the labelsMap key to new_counter
                                main = registersSplit[0]; // string main = label
                                break;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Register Syntax Incorrect!");
                    }
                }
                else if (command.Length == 1)
                {
                    command[0] = command[0].TrimEnd(':');
                    switch (flag)
                    {
                        case Flags.main:
                            
                            break;
                        case Flags.here:
                           
                            break;
                        case Flags.exit:
                            
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("Syntax is not correct! Please check and try again");
                }
                // converts int values of registers to hexaecimals
                string hex = ((int)registersMap[r0]).ToString("x"); string hex1 = ((int)registersMap[r1]).ToString("x"); string hex2 = ((int)registersMap[r2]).ToString("x");
                string hex3 = ((int)registersMap[r3]).ToString("x"); string hex4 = ((int)registersMap[r4]).ToString("x"); string hex5 = ((int)registersMap[r5]).ToString("x");
                string hex6 = ((int)registersMap[r6]).ToString("x"); string hex7 = ((int)registersMap[r7]).ToString("x"); string hex8 = ((int)registersMap[r8]).ToString("x");
                string hex9 = ((int)registersMap[r9]).ToString("x"); string hex10 = ((int)registersMap[r10]).ToString("x"); string hex11 = ((int)registersMap[r11]).ToString("x");
                string hex12 = ((int)registersMap[r12]).ToString("x");                
                
                richTextBox2.AppendText("r0     0x" + hex + "     " + registersMap[r0] + "\r\nr1      0x" + hex1 + "     " + registersMap[r1] + "\r\nr2      0x" + hex2 + "     " + registersMap[r2] +
               "\r\nr3      0x" + hex3 + "     " + registersMap[r3] + "\r\nr4      0x" + hex4 + "     " + registersMap[r4] + "\r\nr5      0x" + hex5 + "     " + registersMap[r5] + "\r\nr6      0x" + hex6 + "     " + registersMap[r6] + "\r\nr7      0x" + hex7 + "     " + registersMap[r7] +
               "\r\nr8      0x" + hex8 + "     " + registersMap[r8] + "\r\nr9      0x" + hex9 + "     " + registersMap[r9] + "\r\nr10      0x" + hex10 + "     " + registersMap[r10] + "\r\nr11      0x" + hex11 + "     " + registersMap[r11] + "\r\nr12      0x" + hex12 + "     " + registersMap[r12] +
                "\r\nsp " + registersMap[r13] + "\r\nlr " + registersMap[r14] + "\r\npc <" + main + "+" + 4 * pcCounter + ">" + "\r\n" + opCode + "\r\n\r\n");
                
                richTextBox1.SelectAll();
                richTextBox1.SelectionBackColor = Color.White; // highlights the whole richTextBox1 
                
                if (multiline[counter].Contains(":"))
                {
                    //highlights the line after labels
                    richTextBox1.Select(richTextBox1.GetFirstCharIndexFromLine(counter+1), richTextBox1.Lines[counter+1].Length);
                }
                else
                {
                    //highlight selected line
                    richTextBox1.Select(richTextBox1.GetFirstCharIndexFromLine(counter), richTextBox1.Lines[counter].Length);
                }
                if (new_counter == counter)
                {
                    counter++;//increment counter when not branching
                    pcCounter++; // increment program counter
                }
                else
                {
                    counter = new_counter; //assign newcounter to counter when branching; line jumps to multiline[counter]
                }
                richTextBox1.SelectionBackColor = Color.LightSkyBlue; //highlights the line being run
            }
            else
            {
                MessageBox.Show("End of Program");
            }

        }
    }
}
