﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Emulator
{
    public partial class Form1 : Form
    {
        private Commands commands = new Commands();
        
        public Form1()
        {
            InitializeComponent();
        }
        
            
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
         
        }
        //method runs the comands in Commands class when the button is clicked
        private void button1_Click(object sender, EventArgs e)
        {
            commands.Multiline(richTextBox1, richTextBox2);
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
        //method for loading a text file when the button is clicked
        private void scan_Click(object sender, EventArgs e)
        {
            OpenFileDialog scan = new OpenFileDialog();
            scan.ShowDialog();
            richTextBox1.Text = System.IO.File.ReadAllText(scan.FileName);
        }
        //method for saving a text file when the button is clicked
        private void save_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.ShowDialog();
            System.IO.File.WriteAllText(save.FileName, richTextBox1.Text);
        }
        //method for clearing texts in the richTextBox when the button is clicked
        private void clear_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear(); // clears the texts in richTextBox1
            richTextBox2.Clear(); // clears the texts in richTextBox2
            commands = new Commands(); // resets the values of the registers to zero
        }

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
           
        }
    }
}
